package com.gmail.leopoldfrederique.tp04_frederique_leopold;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MBDS_HAITI_2021";
    private View btChangerActivite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        Log.d(TAG, "Main Activity");
        Log.d(TAG, "La méthode onCreate est appelée");
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"La méthode onStart est appelée");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"La méthode onResume est appelée");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"La méthode onPause est appelée");
     }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"La méthode onStop est appelée");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,"La méthode onRestart est appelée");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"La méthode onDestroy est appelée");
    }

    private void initViews() {
        btChangerActivite = findViewById(R.id.bt_changer_activite);
        btChangerActivite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent);
                Log.d(TAG, "Vous avez cliqué sur le bouton Changer Acvivité");
            }
        });
    }
}