package com.gmail.leopoldfrederique.tp04_frederique_leopold;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class SecondActivity extends AppCompatActivity {
    private static final String TAG = "MBDS_HAITI_2021";
    private View btFermer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initViews();
        Log.d(TAG, "Second Activity");
        Log.d(TAG, "La méthode onCreate est appelée");
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"La méthode onStart est appelée");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"La méthode onResume est appelée");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"La méthode onPause est appelée");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"La méthode onStop est appelée");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,"La méthode onRestart est appelée");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"La méthode onDestroy est appelée");
    }

    private void initViews() {
        btFermer = findViewById(R.id.bt_fermer);
        btFermer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
                Log.d(TAG, "Vous avez cliqué sur le bouton Fermer");
            }
        });
    }
}