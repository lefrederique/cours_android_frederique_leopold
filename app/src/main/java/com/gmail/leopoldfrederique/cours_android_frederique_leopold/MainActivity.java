package com.gmail.leopoldfrederique.cours_android_frederique_leopold;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getSimpleName();

    Button btnTP01,btnTP02,btnTP03,btnTP04;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnTP01 = findViewById(R.id.btn_TP01);
        btnTP02 = findViewById(R.id.btn_TP02);
        btnTP03 = findViewById(R.id.btn_TP03);
        btnTP04 = findViewById(R.id.btn_TP04);

        // Lancement du TO01 depuis ce bouton
        btnTP01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent webIntent = new Intent("gestion.mbds.CALC");
                startActivity(webIntent);
            }
        });

        // Lancement du TO02 depuis ce bouton
        btnTP02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent webIntent = new Intent("com.gmail.leopoldfrederique.tp02");
                startActivity(webIntent);
            }
        });
        // Lancement du TO03 depuis ce bouton
        btnTP03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent webIntent = new Intent("com.gmail.leopoldfrederique.tp03");
                startActivity(webIntent);
            }
        });
        // Lancement du TO04 depuis ce bouton
        btnTP04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent webIntent = new Intent("com.gmail.leopoldfrederique.tp04");
                startActivity(webIntent);
            }
        });
    }
}