package com.gmail.leopoldfrederique.tpo1_frederique_leopold;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
   
    private Button buttonToast;
    private Button buttonInc;
    private TextView textViewCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonToast = findViewById(R.id.id_butt_toast);
        buttonInc = findViewById(R.id.id_butt_incre);
        textViewCount = findViewById(R.id.id_text_display);
        Integer a =0;
        textViewCount.setText(a.toString());

        buttonToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Click "+textViewCount.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        buttonInc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer value = Integer.parseInt(textViewCount.getText().toString()) +1;
                textViewCount.setText(value.toString());
            }
        });
    }
}
