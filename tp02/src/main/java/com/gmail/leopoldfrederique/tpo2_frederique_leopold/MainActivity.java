package com.gmail.leopoldfrederique.tpo2_frederique_leopold;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MBDS";
    private EditText editTextNb1;
    private EditText editTextNb2;
    private Button buttonCompute;

    private final int COMPUTE_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == COMPUTE_CODE && data != null) {
                String result = data.getStringExtra("result");
                Toast.makeText(this, "Result is " + result, Toast.LENGTH_SHORT).show();
            }
        }else{
            if(resultCode == ComputeActivity.RESULT_MINE && requestCode == COMPUTE_CODE ){

                String message_error = data.getStringExtra("message_error");
                Toast.makeText(this, message_error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Methods that allow to retrieve the instance of views in the layout
     */
    private void initViews() {
        editTextNb1 = findViewById(R.id.nb1);
        editTextNb2 = findViewById(R.id.nb2);
        buttonCompute = findViewById(R.id.compute);
       // buttonCompute.setEnabled(false); // Désactiver le bouton

        // Intercept click on the compute button
        buttonCompute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textNb1 = editTextNb1.getText().toString();
                String textNb2 = editTextNb2.getText().toString();

                if(textNb1 == null || textNb2==null || textNb1.trim().equals("")  || textNb2.trim().equals("") ){
                    Log.d(TAG, "Tous les champs sont obligatoire");

                    Toast.makeText(MainActivity.this, "Tous les champs sont obligatoire", Toast.LENGTH_SHORT).show();

                    if( textNb1 == null || textNb1.trim().equals("") ) {
                        Log.d(TAG, "Le champ nombre 1 est obligatoire");
                        editTextNb1.setError("Le champ nombre 1 est obligatoire") ;
                    }

                    if( textNb2 == null || textNb2.trim().equals("") ) {
                        Log.d(TAG, "Le champ nombre 2 est obligatoire");
                        editTextNb2.setError("Le champ nombre 2 est obligatoire") ;
                    }

                }else   {
                    Log.d(TAG, "Tous les champs sont ramplis");
                    Log.d(TAG, "Valeur du champ  est :"+textNb1.toString());
                    Log.d(TAG, "Valeur du champ 2 est :"+textNb2.toString());

                    Toast.makeText(MainActivity.this, "Tu veux calculer la somme de "+textNb1+" et "+textNb2+" -> "+textNb1+" et "+textNb2+" sont les valeurs des deux champs nb1 et nb2 respectivement", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(MainActivity.this, ComputeActivity.class);
                    intent.putExtra("nb1", textNb1);
                    intent.putExtra("nb2", textNb2);
                    //startActivity(intent);
                    startActivityForResult(intent, COMPUTE_CODE);
                }
            }
        });
    }





}