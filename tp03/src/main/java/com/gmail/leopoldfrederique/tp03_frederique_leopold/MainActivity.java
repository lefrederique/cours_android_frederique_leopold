package com.gmail.leopoldfrederique.tp03_frederique_leopold;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_PHONE_STATE;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getSimpleName();

    Button btnGogle, btnGmail, btnAppel, btnTP01;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnTP01 = findViewById(R.id.btn_TP01);
        btnGogle = findViewById(R.id.btn_Google);
        btnGmail = findViewById(R.id.btn_Gmail);
        btnAppel = findViewById(R.id.btn_Appel);


        btnGmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"leopoldfrederique@gmail.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "TP03 - Cours Android MBDS Haiti 2021");
                intent.setPackage("com.google.android.gm");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                    Log.i(TAG, "Lancement de l'application Gmail");
                } else {
                    Toast.makeText(MainActivity.this, "L'application Gmail n'est pas installée.", Toast.LENGTH_SHORT).show();
                    Log.w(TAG, "L'application Gmail n'est pas installée.\"");
                }
            }
        });


        btnAppel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Vérifie si le matériel peut passer un appel
                PackageManager pm = getPackageManager();
                if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) { // L'appel est possible
                    Intent callIntent = new Intent();
                    callIntent.setData(Uri.parse("tel:" + 28155223));
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        callIntent.setAction(Intent.ACTION_CALL);
                        startActivity(callIntent);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{CALL_PHONE}, 1);
                        } else {
                            callIntent.setAction(Intent.ACTION_DIAL);
                            startActivity(callIntent);
                        }
                    }

                } else {
                    Toast.makeText(MainActivity.this, "Ce matériel ne peut pas passer d'appel", Toast.LENGTH_SHORT).show();
                    Log.w(TAG, "Ce matériel ne peut pas passer d'appel");

                }
            }
        });

        btnGogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.google.com";
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(url));
                startActivity(webIntent);
            }
        });

        btnTP01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent webIntent = new Intent("gestion.mbds.CALC");
                startActivity(webIntent);
            }
        });

    }
}
